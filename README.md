# vue3.0-template-admin
## 已完成功能 | The functionality is complete

- [x] Element Plus
- [x] N+1 多级菜单
- [x] Dashboard
- [x] 表格
- [x] router Tab 选项卡
- [x] 表单
- [x] 图表 :antv or echart
- [x] 二维码生成
- [x] 导入导出 Excel
- [x] 导出 Zip 文件
- [x] 拖拽组件
- [x] 富文本编辑器
- [x] markdown 编辑器
- [x] 个人页
- [x] 登录/注册页
- [x] 404 / 403
- [x] 菜单管理
- [x] 角色管理
- [x] 自定义图标
- [x] 图片拖拽/裁剪
- [x] 支持切换主题色:一键换肤
- [x] 指令权限：v-permisson /全局方法：$permission (参考 tableList.vue)
- [x] 国际化
- [x] 项目看板


## 使用到的插件/库 | Plugin or lib

- **eslint-plugin-vue** [eslint-plugin-vue](https://eslint.vuejs.org/user-guide/#faq)
- **axios** 强大的前端请求库
- **fues.js** [fues.js Fuzzy Search 前端模糊搜索](https://github.com/krisk/Fuse)
- **echart** [echart 数据可视化](http://echarts.apache.org/zh/index.html)
- **antv** [antv 蚂蚁数据可视化](https://antv.vision/zh)
  - antv不好用，被我删掉了
- **xlsx** [xlsx SheetJS ](https://www.npmjs.com/package/xlsx)
- **jszip** [jszip 优秀的前端压缩库 ](https://github.com/Stuk/jszip)
- **mockjs** [mockjs 模拟和交互数据](http://mockjs.com/)
- **wangeditor** [wangeditor 富文本编辑器](https://www.wangeditor.com/doc/)
- **fullcalendar** [fullcalendar 丰富的日历插件](https://github.com/fullcalendar/fullcalendar-example-projects/tree/master/vue3-typescript)

## 快速启动 | Quick Start

```

# 进入项目目录
cd vue3.0-template-admin

# 安装依赖
npm install


# 启动服务
npm run dev

# 打包
npm run build

(如果npm不好用就换成npm）

#front打包部署说明
1，npm run build 得到disk
2，将dist文件夹下的所有内容拷贝至backend/build/output/assets下
3，将assets，yjs，jar等一提打包为ypk




```

## 说明

这个项目不用直接下载下来作为自己的front，然后按需修改即可，不用的在自己项目里面删掉

/view/Search是有一个连接合约引擎并执行合约的示例

datanet相关模块，将build之后的dist文件夹下面的所有内容，拷贝到backend/output/assets/下，和yjs,jar,manifest等一起打包

显示：在合约引擎：http://xxx.node.internetapi.cn/DOIP/contractName/assets即可